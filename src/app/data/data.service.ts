import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class DataService {

	constructor() { }

	private data:any = {};

	public setData(key:string,data:any){
		//Se não existem cria já com a info
		if(this.data[key] == undefined){
			this.data[key] = new BehaviorSubject<any>(data);
		}
		else{
			this.data[key].next(data);
		}
	}

	public getSubject(key:string):BehaviorSubject<any>{
		//Se não existe, cria e retorna ele
		if(this.data[key] == undefined){
			this.data[key] = new BehaviorSubject<any>(undefined);
		}
		return this.data[key];
	}
}
