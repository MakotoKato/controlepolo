import { Injectable } from '@angular/core';
import { DataService } from './../../data/data.service';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './../../api/api.service';

@Injectable({
	providedIn: 'root'
})
export class MainService {

	private subscription = new Subscription();
	
	public raizHierarquia:any = {};
	public raizHierarquiaChanged = new BehaviorSubject<any>(undefined); 

	public hierarchyTreeChanged = new BehaviorSubject<any>(undefined);
	
	private hierarchyTree = {};

	constructor(
		private dataService:DataService,
		private apiService:ApiService
	) {

		this.subscription.add(this.dataService.getSubject('raizHierarquia').subscribe(data=>{
			if(data){
				this.raizHierarquia = data;
				this.hierarchyTree[data.userId] = data;
				this.raizHierarquiaChanged.next(this.raizHierarquia);
			}
		}));

		this.subscription.add(this.dataService.getSubject('galhohierarquia').subscribe(data=>{
			if(data){
				this.hierarchyTree[data.userId] = data;
				this.hierarchyTreeChanged.next(this.hierarchyTree);
			}
		}));
	}

	public getRaizHierarquia():Observable<any>{
		let observable : Observable<any> = Observable.create(observer=>{
			this.raizHierarquiaChanged.subscribe(data=>{
				if(data){
					observer.next(data);
					observer.complete();
				}
			});
		});
		return observable; 
	}

	public getGalhoHierarquia(galhoId):Observable<any>{
		let observable : Observable<any> = Observable.create(observer=>{
			
			if(this.hierarchyTree[galhoId]){
				observer.next(this.hierarchyTree[galhoId]);
				observer.complete();				
			}
			else{

				let payload = {'galho':galhoId};
				this.apiService.post('galhohierarquia',payload).subscribe(data=>{
					if(data) this.dataService.setData('galhohierarquia',data);
				});

				this.hierarchyTreeChanged.subscribe(data=>{
					if(data[galhoId]){
						observer.next(data[galhoId]);
						observer.complete();
					}
				});
			}
		});
		return observable; 
	}
}
