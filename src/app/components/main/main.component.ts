import { Component, OnInit } from '@angular/core';
import { MainService } from './main.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

	public _galho:any;
	public rollclass:string = '';

	constructor(
		private mainService:MainService,
	) { }

	set galho(galho:any){
		
		console.log(galho);
		this._galho = galho;
	}

	get galho():any{
		return this._galho;
	}

	ngOnInit() {
		this.mainService.getRaizHierarquia().subscribe(data=>{
			this.galho = data;
		});
	}

	public view(galhoId:string,animation:string){
		this.mainService.getGalhoHierarquia(galhoId).subscribe(data=>{
			this.galho = data;
			this.finishLoading(animation);
		});
	}

	public finishLoading(animation:string){
		if(animation == 'goUp'){
			this.rollclass = 'slide-out-top';
			setTimeout(()=>{this.rollclass = 'slide-in-bottom';},500);
		}
		else if(animation == 'goDown'){
			this.rollclass = 'slide-out-bottom';
			setTimeout(()=>{this.rollclass = 'slide-in-top';},500);
		}
	}
}
