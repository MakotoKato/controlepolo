import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
declare var getSessionToken : any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
	
	constructor(private appService: AppService) { }

	ngOnInit(){
		this.waitForSessionToken().then(()=>{
			this.appService.init()
		});
	}

	private waitForSessionToken():Promise<void>{
		return new Promise((resolve,reject)=>{
			this.checkToken(resolve,reject,0);
		})
	}

	private checkToken(resolve,reject,tries:number):void{
		if(getSessionToken()){
			resolve();
		}
		else if(tries >= 10){
			reject();
		}
		else{
			setTimeout(()=>{this.checkToken(resolve,reject,tries+1)},1000);
		}
	}
}