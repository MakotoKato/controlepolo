import { Injectable } from '@angular/core';
import { ApiService } from './api/api.service';
import { DataService } from './data/data.service';

declare var getSessionToken : any;

@Injectable({
	providedIn: 'root'
})
export class AppService {

	constructor(
		private apiService:ApiService,
		private dataService:DataService
	) { }

	public init(){
		//Passando as variaveis de amb e windopost
		this.apiService.token = getSessionToken();

		//Primeiras chamadas
		//lider de polo
		this.apiService.post('raizHierarquia',null).subscribe(data=>{
			if(data) this.dataService.setData('raizHierarquia',data);
		});

		//Pocing
		
		//distrital marcelo
		let payload1 = {'galho':'0051L000005LrSMQA0'};
		this.apiService.post('galhohierarquia',payload1).subscribe(data=>{
			if(data) this.dataService.setData('galhohierarquia',data);
		});
		
		//regional andré
		let payload2 = {'galho':'0051L000005LskgQAC'};
		this.apiService.post('galhohierarquia',payload2).subscribe(data=>{
			if(data) this.dataService.setData('galhohierarquia',data);
		});

		//vendedora ana luiza
		let payload3 = {'galho':'0051L000007q8Z2QAI'};
		this.apiService.post('galhohierarquia',payload3).subscribe(data=>{
			if(data) this.dataService.setData('galhohierarquia',data);
		});

		//regional aude germando
		let payload4 = {'galho':'00541000002YY1rAAG'};
		this.apiService.post('galhohierarquia',payload4).subscribe(data=>{
			if(data) this.dataService.setData('galhohierarquia',data);
		});
	}
}