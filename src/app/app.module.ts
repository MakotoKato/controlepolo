//Modules
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

//Core Services
import { AppService } from './app.service';
import { ApiService } from './api/api.service';
import { DataService } from './data/data.service';

//Components
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';

//Component Services
import { MainService } from './components/main/main.service';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    //Core Services
  	AppService,
  	ApiService,
    DataService,
    //Component Services
    MainService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
