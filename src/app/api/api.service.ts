import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ApiService {

	constructor(private http: HttpClient) {}

	private _token;

	set token(token:string){
		this._token = token;
	}

	get token():string{
		return this._token;
	}

	private sfApiControlePoloSettings(){
		return {
			fullEndpoint: 'https://stone--sprint51.cs60.my.salesforce.com/services/apexrest/controledepolo',
			apiURL: 'https://stone--sprint51.cs60.my.salesforce.com/services/apexrest',
			endpoint: '/controledepolo',
			httpOptions: {
				headers: new HttpHeaders({
					'Content-Type':  'application/json',
					'Authorization': 'Bearer '+this.token
				})
			}
		};
	}  

	public post(operation:String,payload:any){
		let body = this.getBody(operation,payload);
		let settings = this.sfApiControlePoloSettings();

		return this.http.post(settings.fullEndpoint, body, settings.httpOptions).pipe(
			//map(this.debug),
			map(this.handleData),
			catchError(this.handleError)
		);
	}

    private debug(data:any){
		console.log(data);
		return data;
    }

    private handleData(data:any){
		if(data['success']){
			return JSON.parse(data['data']);
		}
		else{
			throw new Error("chamada sem sucesso");	
		}
    }

    private handleError(error:any){
        return throwError(error);
    }

	private getBody(operation:String,payload:any){
		return {
    		"operation": operation, 
    		"payload": JSON.stringify(payload),
    	};
	}
}
